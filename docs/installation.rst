============
Installation
============

GBprocesS runs under Linux operating systems. Installation of GBprocesS requires a working version of Python 3.7 or older and pip installed.

Downloading the software
------------------------
The software can be obtained from https://gitlab.com/ilvo/GBprocesS.

* Downloading using the browser, or using ``wget``
* Downloading using the command line (git)::

    git clone https://gitlab.com/ilvo/GBprocesS

Installing GBprocesS in $HOME using pip
---------------------------------------
The package can be installed using the latest version of ``pip3``::
    
    pip install --user gbprocess-ngs

This will install GBprocesS in ``$HOME``. 


Or download an archive (Warning: according config files need to be used)::

    wget https://gitlab.com/ilvo/GBprocesS/-/archive/2.0.2/GBprocesS-2.0.2.tar.gz
    tar -xvf GBprocesS-2.0.2.tar.gz
    mv GBprocesS-2.0.2 GBprocesS

The archived package can be installed using the latest version of ``pip3``::
    
    cd GBprocesS
    pip install --upgrade pip
    pip3 install --user .

This will install GBprocesS in ``$HOME/.local/bin``. 
If an already existing installation is present, first download the newer/latest version and add the ``--upgrade`` parameter to install a newer/latest version::

    wget https://gitlab.com/ilvo/GBprocesS/-/archive/4.0.0.post1/GBprocesS-4.0.0.post1.tar.gz
    tar -xvf GBprocesS-4.0.0.post1.tar.gz
    mv GBprocesS-4.0.0.post1 GBprocesS
    cd GBprocesS
    pip install --upgrade pip
    pip install --upgrade .

Installing GBprocesS in virtual environment using pip
-----------------------------------------------------
The package with dependencies can be installed in virtual environments, allowing these dependencies to be installed without administrator privileges and for a single user only. According to the [python docs](https://docs.python.org/3/tutorial/venv.html), a virtual environment is a self-contained directory tree that contains a Python installation for a particular version of Python, plus a number of additional packages. Creating a virtual environment for python3 is pretty straightforward::

    python3 -m venv <environment_folder_name> 

The above commands will create a hidden folder ``<environment_folder_name>`` which contains the new virtual environment. This local environment has the same structure as the global python environment. For example, a python executable can be found in ``<environment_folder_name>/bin/``. However, it is not necessary to adjust scripts to point to the python executable in this folder. Instead, python virtual environments can be activated to perform this adjustment automatically.


A virtual environment can be activated using::

    source <environment_folder_name>/bin/activate

When activated, the ``<environment_folder_name>/bin/`` folder will be added to the linux PATH. As a consequence, for every python-related operation that the user performs, the activated virtual environment is used. This includes installing and removing software, running python, etc. Environments can also be activated from scripts, making it possible to install software into virtual environments and remove that virtual environment when the script finishes.

For installing python software, ``pip`` is used. By default pip will install packages from the Python Package Index, https://pypi.org. If packages are installed into a virtual environment, no sudo rights are required.
By default, virtual environments can ship outdated pip versions. It is necessary to update pip before you continue, otherwise you might get an error that cython is not installed::

    pip install --upgrade pip
    pip install gbprocess-ngs

After you have finished your analysis in the virtual environment, you leave from the virtual environment by::

    deactivate

Example installation
--------------------
Installation in an virtual environment using pip::

    python3 -m venv .venv
    source .venv/bin/activate
    pip install --upgrade pip
    pip install gbprocess-ngs


Installation in an virtual environment using Git::

    git clone https://gitlab.com/ilvo/GBprocesS
    cd GBprocesS
    python3 -m venv .venv
    source .venv/bin/activate
    pip install --upgrade pip
    pip install .

Or::

    git clone https://gitlab.com/ilvo/GBprocesS ; cd GBprocesS ; python3 -m venv .venv ; source .venv/bin/activate ; pip install --upgrade pip ; pip install .


After the installation, test if the program is correctly installed by using::

    gbprocess --help


Exit the virtual environment with::

    deactivate
